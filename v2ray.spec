Name:           v2ray
Version:        4.18.2
Release:        1%{?dist}
Summary:        The core of the network forwarding toolkit Project V

License:        MIT
URL:            https://github.com/%{name}/%{name}-core
Source0:        https://github.com/%{name}/%{name}-core/archive/v%{version}/%{name}-core-v%{version}.tar.gz
Source1:        v2ray.xml
Source4:        v2ray@.service
Source5:        v2ray.tmpfile
Source6:        vpoint_client_socks_shadowsocks.json
Source7:        vpoint_client_socks_websocket_tls.json
Source8:        vpoint_server_websocket_tls.json
Source9:        vpoint_server_websocket_tls_nginx_vhost.conf.example
BuildRequires:  go >= 1.11.5-1.2
BuildRequires:  git >= 2.14.4-1
BuildRequires:  gcc
BuildRequires:  systemd
Requires:       systemd
Requires(pre):  shadow
ExclusiveArch:  x86_64
BuildRoot:      %{_tmppath}/%{name}-%{version}-build


%description
The core of Project V, which is a series of tools for network forwarding. 

%package firewalld
Summary: The firewalld service file.
%description firewalld
The functional firewalld service file.

%global debug_package %{nil}

%prep
%setup -q -n %{name}-core-%{version}
cp %{_sourcedir}/%{name}.xml ./

%build
export GO111MODULE=on
go build -o v2ray -compiler gc -gcflags -trimpath=%{gopath} -asmflags -trimpath=%{gopath} -ldflags "-s -w" ./main
go build -o v2ctl -compiler gc -gcflags -trimpath=%{gopath} -asmflags -trimpath=%{gopath} -ldflags "-s -w" ./infra/control/main

%install
install -d %{buildroot}%{_bindir}/%{name}
install -d %{buildroot}%{_sysconfdir}/%{name}
install -d %{buildroot}%{_usr}/lib/firewalld/services/
install -d %{buildroot}%{_localstatedir}/log/%{name}
install -d %{buildroot}%{_unitdir}

install -p -m 0755 %{name} %{buildroot}%{_bindir}/%{name}/%{name}
install -p -m 0755 v2ctl %{buildroot}%{_bindir}/%{name}/v2ctl
install -p -m 0644 $(pwd)/release/config/geoip.dat %{buildroot}%{_bindir}/%{name}/geoip.dat
install -p -m 0644 $(pwd)/release/config/geosite.dat %{buildroot}%{_bindir}/%{name}/geosite.dat
install -p -m 0644 $(pwd)/release/config/systemd/%{name}.service %{buildroot}%{_unitdir}/%{name}.service

install -p -m 0644 $(pwd)/release/config/vpoint_vmess_freedom.json %{buildroot}%{_sysconfdir}/%{name}/config.json
install -p -m 0644 $(pwd)/v2ray.xml %{buildroot}%{_usr}/lib/firewalld/services/v2ray.xml

install -m 0644 %{S:6} %{buildroot}%{_sysconfdir}/v2ray/
install -m 0644 %{S:7} %{buildroot}%{_sysconfdir}/v2ray/
install -m 0644 %{S:8} %{buildroot}%{_sysconfdir}/v2ray/
install -m 0644 %{S:9} %{buildroot}%{_sysconfdir}/v2ray/
install -m 0644 %{S:5} %{buildroot}%{_libexecdir}/tmpfiles.d/v2ray.conf

%files
%defattr(-,root,root,-)
%license LICENSE
%{_bindir}/%{name}/%{name}
%{_bindir}/%{name}/v2ctl
%{_bindir}/%{name}/geoip.dat
%{_bindir}/%{name}/geosite.dat
%attr(-, root, root) %{_unitdir}/%{name}.service
%config(noreplace) %{_sysconfdir}/%{name}/config.json
%attr(-, root, root) %{_localstatedir}/log/%{name}
%config(noreplace) %attr(-,v2ray,v2ray) %{_sysconfdir}/v2ray/vpoint_client_socks_shadowsocks.json
%config(noreplace) %attr(-,v2ray,v2ray) %{_sysconfdir}/v2ray/vpoint_client_socks_websocket_tls.json
%config(noreplace) %attr(-,v2ray,v2ray) %{_sysconfdir}/v2ray/vpoint_server_websocket_tls.json
%config(noreplace) %attr(-,v2ray,v2ray) %{_sysconfdir}/v2ray/vpoint_server_websocket_tls_nginx_vhost.conf.example
%{_libexecdir}/tmpfiles.d/v2ray.conf
%files firewalld
%config(noreplace) %{_usr}/lib/firewalld/services/v2ray.xml


%changelog
* Sat Mar 02 2019 rme <rikka.muromi@yahoo.com> - 4.18.0-1
- Update to v4.18.0

* Fri Dec 14 2018 rme <rikka.muromi@yahoo.com> - 4.9.0-1
- Update to v4.9.0

* Sat Nov 24 2018 rme <rikka.muromi@yahoo.com> - 4.6.0-1
- Update to v4.6.0

* Mon Nov 12 2018 rme <rikka.muromi@yahoo.com> - 4.3-1
- Update to v4.3

* Fri Nov 2 2018 rme <rikka.muromi@yahoo.com> - 4.0-1
- Update to v4.0

* Fri Oct 26 2018 rme <rikka.muromi@yahoo.com> - 3.49-1
- Update to v3.49

* Thu Oct 11 2018 rme <rikka.muromi@yahoo.com> - 3.46-1
- Update to v3.46

* Thu Oct 04 2018 rme <rikka.muromi@yahoo.com> - 3.45-1
- Update to v3.45

* Mon Sep 24 2018 rme <rikka.muromi@yahoo.com> - 3.43-1
- Update to v3.43

* Sat Sep 15 2018 rme <rikka.muromi@yahoo.com> - 3.41-1
- Update to v3.41

* Fri Aug 31 2018 rme <rikka.muromi@yahoo.com> - 3.37-1
- Update to v3.37

* Mon Aug 27 2018 rme <rikka.muromi@yahoo.com> - 3.36-1
- Rebuild
